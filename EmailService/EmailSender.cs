﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EmailService
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailConfiguration;

        public EmailSender(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        /// <summary>
        /// Envia mensaje
        /// </summary>
        /// <param name="message">Mensaje a enviar</param>
        public void SendMessage(Message message)
        {
            var emailMessage = CreateEmailMessage(message);

            Send(emailMessage);
        }

        /// <summary>
        /// Envia mensaje de forma asincrona
        /// </summary>
        /// <param name="message">Mensaje a enviar</param>
        /// <returns></returns>
        public async Task SendMessageAsync(Message message)
        {
            var emailMessage = CreateEmailMessage(message);

           await SendAsync(emailMessage);
        }

        /// <summary>
        /// Envia mensaje con archivos adjuntos de forma asincrona
        /// </summary>
        /// <param name="message">Mensaje</param>
        /// <param name="streamsFiles">archivos adjuntos</param>
        /// <returns></returns>
        public async Task SendMessageAsync(Message message, Dictionary<string, MemoryStream> streamsFiles)
        {
            var emailMessage = CreateEmailMessage(message, streamsFiles);

            await SendAsync(emailMessage);
        }

        private MimeMessage CreateEmailMessage(Message message) {
            var mimessage = new MimeMessage();
          
            mimessage.From.Add(new MailboxAddress(_emailConfiguration.From));
            mimessage.To.AddRange(message.To);
            mimessage.Subject = message.Subject;
            var bodyBuilder = new BodyBuilder { HtmlBody = string.Format("<h2 style='color:blue'>{0}</h2>", message.Content) };

            if (message.Attachments != null && message.Attachments.Any()) {
                byte[] fileBytes;
                foreach (var attachment in message.Attachments)
                {
                    using (var ms = new MemoryStream())
                    {
                        attachment.CopyTo(ms);
                        fileBytes = ms.ToArray();
                    }
                    bodyBuilder.Attachments.Add(attachment.FileName, fileBytes, ContentType.Parse(attachment.ContentType));
                }
            }
            mimessage.Body = bodyBuilder.ToMessageBody();

            return mimessage;
        }

        private MimeMessage CreateEmailMessage(Message message, Dictionary<string, MemoryStream> streamsFiles)
        {
            var mimessage = new MimeMessage();

            mimessage.From.Add(new MailboxAddress(_emailConfiguration.From));
            mimessage.To.AddRange(message.To);
            mimessage.Subject = message.Subject;
            var bodyBuilder = new BodyBuilder { HtmlBody = string.Format("<h2 style='color:blue'>{0}</h2>", message.Content) };

            if (streamsFiles != null && streamsFiles.Any())
            {
                byte[] fileBytes;
                foreach (var attachment in streamsFiles)
                {
                    fileBytes = attachment.Value.ToArray();
                    bodyBuilder.Attachments.Add(attachment.Key, fileBytes, ContentType.Parse(UtilitiesGeneric.GetContentType(attachment.Key)));
                }
            }
            mimessage.Body = bodyBuilder.ToMessageBody();

            return mimessage;
        }

        private void Send(MimeMessage message) {
            using (var client = new SmtpClient())
            {
                try {
                    client.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_emailConfiguration.UserName, _emailConfiguration.Password);
                    client.Send(message);
                }finally {
                    client.Disconnect(true); client.Dispose();
                }
            }
        }

        private async Task SendAsync(MimeMessage message)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_emailConfiguration.UserName, _emailConfiguration.Password);

                    await client.SendAsync(message);
                }
                finally
                {
                    await client.DisconnectAsync(true); client.Dispose();
                }
            }
        }
    }
}
