﻿using Microsoft.AspNetCore.Http;
using MimeKit;
using System.Collections.Generic;
using System.Linq;

namespace EmailService
{
    public class Message
    {
        public List<MailboxAddress> To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public IFormFileCollection Attachments { get; set; }

        public Message(IEnumerable<string> to, string subject, string content) {
            To = new List<MailboxAddress>();

            To = to.Select(x => new MailboxAddress(x)).ToList();
            Subject = subject;
            Content = content;
        }
    }
}
