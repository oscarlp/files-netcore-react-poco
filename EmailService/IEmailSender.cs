﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace EmailService
{
    public interface IEmailSender
    {
        /// <summary>
        /// Envia mensaje
        /// </summary>
        /// <param name="message">Mensaje a enviar</param>
        void SendMessage(Message message);

        /// <summary>
        /// Envia mensaje de forma asincrona
        /// </summary>
        /// <param name="message">Mensaje a enviar</param>
        /// <returns></returns>
        Task SendMessageAsync(Message message);

        /// <summary>
        /// Envia mensaje con archivos adjuntos de forma asincrona
        /// </summary>
        /// <param name="message">Mensaje</param>
        /// <param name="streamsFiles">archivos adjuntos</param>
        /// <returns></returns>
        Task SendMessageAsync(Message message, Dictionary<string, MemoryStream> streamsFiles);
    }
}
