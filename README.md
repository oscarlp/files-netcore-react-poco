# EmailServiceLib

Repo own library to send emails.

Have a class EmailSender and a interface IEmailSender

## Installation
Install searching in Nuget library like EmailServiceLib

## Usage
Implement like a new class
Define your configuration json object:

     "EmailConfiguration": { 
       "From": "youremail@gmail.com",
       "SmtpServer": "yoursmtp",
       "Port": 123,
       "Username": "email",
       "Password": "emailpassword"
     }

     private readonly IEmailSender _emailSender = null;
     _emailSender = new EmailSender(_config.GetSection("EmailConfiguration").Get<EmailConfiguration>());

Use.
        //Methods
         var message=new Message(new string[] { email }, "Recover pass", $"Your code to recover account is: <b>{code}<b/>");
         await _emailSender.SendMessageAsync(message);

         Dictionary<string, MemoryStream> memoriesStream = new Dictionary<string, MemoryStream>()
         memoriesStream.Add(filename, <"here going filestream value">)
         await _emailSender.SendMessageAsync(message, memoriesStream);



