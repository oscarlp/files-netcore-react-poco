﻿using FilesPoco.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FilesPoco.Business
{
    public class FileBusiness
    {
        // private readonly ISqlLiteConnec _connection;
       
        public FileBusiness() { 
        
        }

        public async Task<bool> SaveFile(FileDTO file, string route)
        {
            try
            {
                if (!Directory.Exists(route))
                    Directory.CreateDirectory(route);

                string path = Path.Combine(route, file.FileName);

                if (File.Exists(path))
                    throw new ArgumentException("File name already exists");

                //save in folder path
                using (Stream stream = new FileStream(path, FileMode.Create))
                {
                   await file.FormFile.CopyToAsync(stream);
                }

                return true;       
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<object>> List(string path) {
            try
            {
                List<object> files = new List<object>();

                string[] dirs = Directory.GetFiles(path);
                await Task.Run(() =>
                {
                    foreach (string dir in dirs)
                    {
                        var stream = new FileStream(dir, FileMode.Open, FileAccess.Read);
                        using (var sr = new StreamReader(stream))
                        {
                            files.Add(new { id = new Random().Next(1, 1000), fileName = Path.GetFileName(dir), Size = Decimal.Round(Convert.ToDecimal((stream.Length/1024f)/1024f), 2) });
                        }
                    }
                });
               
                return files;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task<MemoryStream> GetFile(string fileNameWithExtension, string route)
        {
            try
            {
                if (!Directory.Exists(route))
                    throw new ArgumentException("Route not exists");

                string path = Path.Combine(route, fileNameWithExtension);

                if (!File.Exists(path))
                    throw new ArgumentException("File name not exists");

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return memory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Dictionary<string,MemoryStream>> GetFiles(List<string> fileNamesWithExtension, string route)
        {
            try
            {
                if (!Directory.Exists(route))
                    throw new ArgumentException("Route not exists");

                Dictionary<string, MemoryStream> memoriesStream = new Dictionary<string, MemoryStream>();
                foreach (var filename in fileNamesWithExtension)
                {
                    memoriesStream.Add(filename,await GetFile(filename, route));
                }
                return memoriesStream;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Dictionary<string, string>> DeleteFiles(List<string> fileNames, string route)
        {
            try
            {
                Dictionary<string, string> filesResult = new Dictionary<string, string>();
                foreach (var item in fileNames)
                {
                    try {
                        var resp = await DeleteFile(item, route);
                        if (resp) filesResult.Add(item, "OK");
                        else filesResult.Add(item, "Error unknow deleting");
                    } catch (Exception ex) {
                        filesResult.Add(item, ex.Message);
                    }              
                }
                return filesResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> DeleteFile(string fileNameWithExtension, string route)
        {
            try
            {
                if (!Directory.Exists(route))
                    throw new ArgumentException("Route not exists");

                string path = Path.Combine(route, fileNameWithExtension);

                if (!File.Exists(path))
                    throw new ArgumentException("File name not exists");

                File.Delete(path);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
