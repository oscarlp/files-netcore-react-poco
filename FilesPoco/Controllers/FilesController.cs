﻿using EmailService;
using FilesPoco.Business;
using FilesPoco.DTO;
using FilesPoco.Helpers;
using FilesPoco.Properties;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FilesPoco.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FilesController : Controller
    {
        //Aplicacion simple
        private readonly FileBusiness _fileBusiness;
        private readonly IEmailSender _emailSender;
        public FilesController(IEmailSender emailSender) {
            _fileBusiness = new FileBusiness();
            _emailSender = emailSender;
        }

        [HttpPost, Route(nameof(FilesController.Import))]
        public async Task<JsonResult> Import([FromForm]FileDTO file) {
            try
            {
                var resp = await _fileBusiness.SaveFile(file, Path.Combine(Directory.GetCurrentDirectory(),Resources.DefaultPdfRoute));

                return new JsonResult(new
                {
                    response = true,
                    data = new {Id= new Random().Next(1,1000), file.FileName, Size= file.FormFile.Length},
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet, Route(nameof(FilesController.List))]
        public async Task<JsonResult> List()
        {
            try
            {
                var resp = await _fileBusiness.List(Path.Combine(Directory.GetCurrentDirectory(), Resources.DefaultPdfRoute));
               
                return new JsonResult(new
                {
                    response = true,
                    data = resp,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }


        [HttpGet, Route(nameof(FilesController.Export)+"/{filename}")]
        public async Task<IActionResult> Export([FromRoute]string filename)
        {
            try
            {
                var resp = await _fileBusiness.GetFile(filename, Path.Combine(Directory.GetCurrentDirectory(), Resources.DefaultPdfRoute));
                return File(resp, Utilities.GetContentType(filename), Path.GetFileName(filename));
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }


        [HttpPost, Route(nameof(FilesController.Delete))]
        public async Task<JsonResult> Delete(List<string> filesname)
        {
            try
            {
                var resp = await _fileBusiness.DeleteFiles(filesname, Path.Combine(Directory.GetCurrentDirectory(), Resources.DefaultPdfRoute));
                return new JsonResult(new
                {
                    response = true,
                    data = resp,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }

        [HttpPost, Route(nameof(FilesController.SendEmail))]
        public async Task<JsonResult> SendEmail([FromBody]EmailFileDTO emailFile) {
            try
            {
                var message = new Message(new string[] { emailFile.EmailTo }, "FilesPoco backend", "This is a email test async");
                await _emailSender.SendMessageAsync(message, await _fileBusiness.GetFiles(emailFile.FilesName, Resources.DefaultPdfRoute));
                return new JsonResult(new
                {
                    response = true,
                    data = true,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    response = false,
                    data = false,
                    error = ex.Message
                });
            }
        }

    }
}
