﻿using System.Collections.Generic;

namespace FilesPoco.DTO
{
    public class EmailFileDTO
    {
        public List<string> FilesName { get;set; }
        public string EmailTo { get; set; }
    }
}
