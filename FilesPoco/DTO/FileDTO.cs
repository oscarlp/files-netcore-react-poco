﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace FilesPoco.DTO
{
    [DataContract]
    public class FileDTO
    {
        [Required]
        /// <summary>
        /// <function>Name of file without extension</function>
        /// <example>MyFile</example>
        /// </summary>
        public string FileName { get; set; }

        [Required]
        /// <summary>
        /// <function>Data of file</function>
        /// <example></example>
        /// </summary>
        public IFormFile FormFile { get; set; }
    }
}
