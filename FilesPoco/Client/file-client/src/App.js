import React, {useState, useEffect} from 'react'
import './AppStyle.js'
import styles from './AppStyle.js'
import {Button, TextField} from '@material-ui/core'
import SendIcon from '@material-ui/icons/Send'
import CloseIcon from '@material-ui/icons/Close'
import DeleteIcon from '@material-ui/icons/Delete'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import { DataGrid } from '@material-ui/data-grid'
import { makeStyles } from '@material-ui/core/styles'
import FileService from './Services/FileService'

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'fileName', headerName: 'File name', width: 300 },
  { field: 'size', headerName: 'Size (MB)', width: 120 }
];

function App() {
  //#region Build
  const [file, setFile]= useState(null);
  const [fileName, setFileName]= useState('');
  const [email, setEmail] = useState('');
  const [sms, setSms] = useState('');
  const [showOptions, setShowOptions] = useState(false);
  const [rows, setRows] = useState([]);
  const [selectionModel, setSelectionModel] = useState([]);
  const [filesSelected, setFilesSelected] = useState([]);

  useEffect(async()=>{
    LoadList();  
  },[setRows]);

  const LoadList=async()=>{
    let list= await FileService.ListFile();
    setRows(list || []);
  }

  const useStyles = makeStyles({
    button: {
      marginBottom :10,
      marginTop: 10,
      marginLeft: 20
    },
  });
  const classes = useStyles();
  //#endregion

  //#region Methods
  const uploadFile = async(e) =>{
    setFile(e.target.files[0]);
    setFileName(e.target.files[0].name);
  }

  const saveFile =async(e)=> {
    let formData = new FormData();
    formData.append("formFile", file);
    formData.append("fileName", fileName);
    await FileService.SaveFile(formData);
    document.getElementById("inputfile").value = ""; setFileName('');
    LoadList();    
  }

  const deleteFiles=async(e)=>{
    await FileService.DeleteFiles(filesSelected);
    LoadList();
  }

  const downloadFile=async(e)=>{
    await FileService.DownloadFile(filesSelected[0]);//por ahora solo descarga primer archivo seleccionado
  }

  const sendEmailFiles=async(e)=>{
    await FileService.SendEmailFiles(filesSelected, email);
  }

  const updateFileSelected=(e)=>{
    let files= (filesSelected.includes(e.data.fileName) && !e.isSelected) ? filesSelected.filter(x=> x!==e.data.fileName) : [...filesSelected, e.data.fileName];
    setFilesSelected(files);
  }
  //#endregion

  //#region Render view
  return (
    <div style={styles.App}>
      <div style={styles.FileSection}>
          <input type="file" id="inputfile" onChange={uploadFile}/>
          <Button variant="contained" color="primary" className={classes.button} onClick={(e)=>{saveFile(e)}}>Save</Button>
          <p>{fileName}</p>
      </div>
      <div style={styles.TableSection}>
        <div style={{ height: 400, width: '100%' }}> 
          <Button variant="contained" style={styles.BtnDel} startIcon={<DeleteIcon/>} disabled={!(selectionModel.length>0)} onClick={(e)=>{deleteFiles(e)}}></Button>
          <Button variant="contained" color="primary" startIcon={<CloudDownloadIcon/>} disabled={!(selectionModel.length>0)} onClick={(e)=>{downloadFile(e)}}></Button>
          <DataGrid rows={rows} columns={columns} pageSize={5}  checkboxSelection onSelectionModelChange={(e) => {setSelectionModel(e.selectionModel)}} 
          selectionModel={selectionModel} onRowSelected={(e)=>{updateFileSelected(e)}}/>
        </div>      
        {showOptions ?
          <div style={{paddingTop:'20'}}>
            <div style={styles.DivBtnClose}>
              <Button variant="contained" style={styles.BtnRad} onClick={(e)=> setShowOptions(false)}><CloseIcon /></Button>
            </div>
            <div>
              <TextField error= {false} id="email" label="Email..." defaultValue={email} helperText="" onChange={e=>{setEmail(e.target.value)}}/>
              <Button variant="contained" color="primary"  disabled={!(selectionModel.length>0)} className={classes.button} startIcon={<SendIcon/>} onClick={(e)=>{sendEmailFiles(e)}}>Email</Button>
            </div>
            <div>
              <TextField error= {false} id="sms" label="Telephone..." defaultValue={sms} helperText=""/>
              <Button variant="contained" color="secondary" className={classes.button} startIcon={<SendIcon/>}>SMS</Button>
            </div>
          </div> 
          :
          <div>
            <Button variant="contained" color="primary" className={classes.button} onClick={(e)=>{setShowOptions(true)}}>Send email</Button>
            <Button variant="contained" color="secondary" className={classes.button} onClick={(e)=>{setShowOptions(true)}}>Send sms</Button>
          </div>}
      </div>
    </div>
  );
  //#endregion
}

export default App;
