const App = {
  height: '100%',
  width: '100%',
  position: 'absolute',
  alignItems: 'center',
  backgroundColor: '#282c34',
  justifyContent: 'center',
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap'
}
const FileSection ={
  backgroundColor: 'white',
  textAlign: 'center',
  width: '50%',
  maxWidth: '50%',
  marginBottom: '10'
}
const TableSection ={
  backgroundColor: 'white',
  textAlign: 'center',
  width: '70%',
  maxWidth: '70%',
  padding: '2%'
}
const DivBtnClose={
  marginTop: '2%',
  marginBottom: '1%'
}
const BtnSend={
  marginLeft: '20'
}
const BtnRad= {
  borderRadius: '50%',
  backgroundColor: '#ff1744'
}
const BtnDel={
  backgroundColor: '#ff1744'
}

const styles = {
  App,
  FileSection,
  TableSection,
  DivBtnClose,
  BtnSend,
  BtnRad,
  BtnDel
}

export default styles;
