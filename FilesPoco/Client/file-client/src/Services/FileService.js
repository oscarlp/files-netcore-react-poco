import axios from 'axios'
const ListFile=async()=>{
    try{
        const controller = new AbortController();
            const id = setTimeout(() => controller.abort(), 7000);
            let axiosResp = await axios.get('http://localhost:53275/api/v1/Files/List');
            clearTimeout(id);

        if(axiosResp.status==401)
            alert("User or passsword invalid");

        if(axiosResp.status==404)
            alert("Service, URL not found");

        if(axiosResp.status==400)
            alert("Service denied - Bad request");

        if(axiosResp.status==200 && axiosResp.data.response === true && axiosResp.data.error==="")
            return axiosResp.data.data;
        else
            alert("Error axiosResp Service - "+ axiosResp.data.error); 
        
        return null;
    }catch(err){
        alert(err);
        return null;
    }
}

const SaveFile=async(data)=>{
    try{
        const controller = new AbortController();
            const id = setTimeout(() => controller.abort(), 7000);
            let axiosResp = await axios.post('http://localhost:53275/api/v1/Files/Import', data);
            clearTimeout(id);

        if(axiosResp.status==401)
            alert("User or passsword invalid");

        if(axiosResp.status==404)
            alert("Service, URL not found");

        if(axiosResp.status==400)
            alert("Service denied - Bad request");

        if(axiosResp.status==200 && axiosResp.data.response === true && axiosResp.data.error==="")
            return axiosResp.data.data;
        else
            alert("Error axiosResp Service - "+ axiosResp.data.error); 
        
        return null;
    }catch(err){
        alert(err);
        return null;
    }
}

const DownloadFile=async(fileName)=>{
    try{
        const controller = new AbortController();
        const id = setTimeout(() => controller.abort(), 7000);
        let axiosResp = await axios.get('http://localhost:53275/api/v1/Files/Export/'+fileName, {responseType:'blob'});
        clearTimeout(id);

        if(axiosResp.status==401)
            alert("User or passsword invalid");

        if(axiosResp.status==404)
            alert("Service, URL not found");

        if(axiosResp.status==400)
            alert("Service denied - Bad request");

        if(axiosResp.status==200){
            let url = window.URL.createObjectURL(new Blob([axiosResp.data]));
            let link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
        }

    }catch(err){
        alert(err);
        return null;
    }
}

const DeleteFiles=async(data)=>{
    try{
        const controller = new AbortController();
            const id = setTimeout(() => controller.abort(), 7000);
            let axiosResp = await axios.post('http://localhost:53275/api/v1/Files/Delete', data);
            clearTimeout(id);

        if(axiosResp.status==401)
            alert("User or passsword invalid");

        if(axiosResp.status==404)
            alert("Service, URL not found");

        if(axiosResp.status==400)
            alert("Service denied - Bad request");

        if(axiosResp.status==200 && axiosResp.data.response === true && axiosResp.data.error===""){
            console.log(axiosResp.data.data);
            let errorsDelFiles = data.filter(x=>axiosResp.data.data[x] !== "OK");
            if(errorsDelFiles.length>0) alert("Files error deleting: "+ errorsDelFiles.join(' , '))
        }
        else{
            alert("Error axiosResp Service - "+ axiosResp.data.error); 
        }
        
        return null;
    }catch(err){
        alert(err);
        return null;
    }
}

const SendEmailFiles=async(data, email)=>{
    try{
        const controller = new AbortController();
        const id = setTimeout(() => controller.abort(), 7000);
        let axiosResp = await axios.post('http://localhost:53275/api/v1/Files/SendEmail', {FilesName:data, EmailTo: email});
        clearTimeout(id);

        if(axiosResp.status==401)
            alert("User or passsword invalid");

        if(axiosResp.status==404)
            alert("Service, URL not found");

        if(axiosResp.status==400)
            alert("Service denied - Bad request");

        if(axiosResp.status==200 && axiosResp.data.response === true && axiosResp.data.error===""){
            alert('Sended files to: '+email);
        }
        else{
            alert("Error axiosResp Service - "+ axiosResp.data.error); 
        }
        
        return null;
    }catch(err){
        alert(err);
        return null;
    }
}

const FileService ={
    SaveFile,
    ListFile,
    DeleteFiles,
    DownloadFile,
    SendEmailFiles
}

export default FileService;